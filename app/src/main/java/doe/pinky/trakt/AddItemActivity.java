package doe.pinky.trakt;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import doe.pinky.trakt.models.Item;
import doe.pinky.trakt.models.User;

public class AddItemActivity extends AppCompatActivity {

    private final static int BAR_CODE_REQUEST_CODE = 99;

    @Bind(R.id.item_input_wrapper)
    TextInputLayout itemInputWrapper;

    @Bind(R.id.item_input)
    EditText itemInput;

    @Bind(R.id.serial_number_input)
    EditText serialNumberInput;

    @Bind(R.id.comments_input)
    EditText commentsInput;

    @Bind(R.id.scan_barcode)
    ImageButton scanBarcode;

    ProgressDialog progressDialog;

    String[] names;
    Map<String, ParseUser> objectIdMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verify();

                Item item = new Item();
                item.setItemName(itemInput.getText().toString());
                item.setSerialNumber(serialNumberInput.getText().toString());
                item.setComments(commentsInput.getText().toString());
                item.setAssignee((User) ParseUser.getCurrentUser());

                ParseACL acl = new ParseACL();
                acl.setPublicReadAccess(true);
                acl.setPublicWriteAccess(false);
                acl.setWriteAccess(ParseUser.getCurrentUser(), true);

                item.setACL(acl);

                progressDialog = new ProgressDialog(AddItemActivity.this);
                progressDialog.setMessage("Saving...");
                progressDialog.setIndeterminate(true);
                progressDialog.setCancelable(false);
                progressDialog.show();

                item.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        progressDialog.dismiss();

                        if (e == null) {
                            Toast.makeText(AddItemActivity.this, "New item was added and assigned to you", Toast.LENGTH_SHORT).show();
                            Intent data = new Intent();
                            setResult(RESULT_OK, data);
                            finish();
                        } else {
                            Toast.makeText(AddItemActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        scanBarcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddItemActivity.this, BarcodeCaptureActivity.class);
                startActivityForResult(intent, BAR_CODE_REQUEST_CODE);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BAR_CODE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                String returnedResult = data.getData().toString();
                serialNumberInput.setText(returnedResult);
            }
        }
    }

    private boolean verify() {
        boolean valid = true;

        if (itemInput.getText().toString().equals("")) {
            itemInputWrapper.setError("Item name is required");
            valid = false;
        } else {
            itemInputWrapper.setError(null);
        }

        return valid;
    }

}
