package doe.pinky.trakt;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseObject;
import com.parse.ParseUser;

import doe.pinky.trakt.models.Item;
import doe.pinky.trakt.models.User;

public class TraktApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        ParseUser.registerSubclass(User.class);
        ParseObject.registerSubclass(Item.class);
        Parse.initialize(this);
    }
}
