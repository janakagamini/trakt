package doe.pinky.trakt;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.GetCallback;
import com.parse.ParseACL;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import doe.pinky.trakt.models.Item;
import doe.pinky.trakt.models.User;

public class ViewItemActivity extends AppCompatActivity {

    @Bind(R.id.progress_view)
    View progressView;

    @Bind(R.id.root_view)
    View rootView;

    @Bind(R.id.item_name)
    TextView itemName;

    @Bind(R.id.serial_number)
    TextView serialNumber;

    ParseQueryAdapter<User> adapter;
    @Bind(R.id.user_list)
    ListView userListView;

    @Bind(R.id.cancel_button)
    Button cancelButton;

    @Bind(R.id.ok_button)
    Button okButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_item);
        ButterKnife.bind(this);

        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        setTitle("Re-assign Item");

        String objectId = getIntent().getStringExtra(Item.OBJECT_ID);

        ParseQuery<Item> query = new ParseQuery<>(Item.CLASS_NAME);
        query.whereEqualTo(Item.OBJECT_ID, objectId);
        query.include(Item.ASSIGNEE);
        query.getFirstInBackground(new GetCallback<Item>() {
            @Override
            public void done(final Item item, ParseException e) {

                if (e == null) {


                    itemName.setText(item.getItemName());

                    if (!item.getSerialNumber().equals(""))
                        serialNumber.setText(item.getSerialNumber());
                    else serialNumber.setText("Serial Number: N/A");

                    adapter = new ParseQueryAdapter<>(ViewItemActivity.this, User.CLASS_NAME, R.layout.item_user_view);
                    adapter.setTextKey(User.NAME);
                    adapter.addOnQueryLoadListener(new ParseQueryAdapter.OnQueryLoadListener<User>() {
                        @Override
                        public void onLoading() {

                        }

                        @Override
                        public void onLoaded(List<User> objects, Exception e) {
                            String assigneeId = item.getAssignee().getObjectId();

                            for (int i = 0; i < adapter.getCount(); i++) {
                                User user = adapter.getItem(i);
                                if (assigneeId.equals(user.getObjectId())) {
                                    userListView.setItemChecked(i, true);
                                    okButton.setEnabled(false);
                                    break;
                                }
                            }

                            progressView.setVisibility(View.GONE);
                            rootView.setVisibility(View.VISIBLE);

                        }
                    });
                    userListView.setAdapter(adapter);
                    userListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            String selectedUserId = adapter.getItem(position).getObjectId();
                            String currentUserId = User.getCurrentUser().getObjectId();

                            if (selectedUserId.equals(currentUserId)) okButton.setEnabled(false);
                            else okButton.setEnabled(true);
                        }
                    });

                    okButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final User selectedUser = adapter.getItem(userListView.getCheckedItemPosition());
                            item.setPrevAssignee(item.getAssignee());
                            item.setAssignee(selectedUser);

                            ParseACL acl = new ParseACL();
                            acl.setPublicReadAccess(true);
                            acl.setPublicWriteAccess(false);
                            acl.setWriteAccess(selectedUser, true);

                            item.setACL(acl);

                            rootView.setVisibility(View.GONE);
                            progressView.setVisibility(View.VISIBLE);

                            item.saveInBackground(new SaveCallback() {
                                @Override
                                public void done(ParseException e) {

                                    if (e == null) {
                                        Toast.makeText(ViewItemActivity.this, item.getItemName() + " re-assigned to: " + selectedUser.getName(), Toast.LENGTH_SHORT).show();
                                        Intent data = new Intent();
                                        setResult(RESULT_OK, data);
                                        finish();
                                    } else {
                                        progressView.setVisibility(View.GONE);
                                        rootView.setVisibility(View.VISIBLE);
                                        Toast.makeText(ViewItemActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                    });

                } else {
                    Toast.makeText(ViewItemActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                    finish();
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
