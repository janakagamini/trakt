package doe.pinky.trakt.models;

import com.parse.ParseClassName;
import com.parse.ParseObject;

@ParseClassName("Item")
public class Item extends ParseObject {

    public static final String CLASS_NAME = "Item";
    public static final String OBJECT_ID = "objectId";
    public static final String UPDATED_AT = "updatedAt";
    public static final String ITEM_NAME = "itemName";
    public static final String SERIAL_NUMBER = "serialNumber";
    public static final String COMMENTS = "comments";
    public static final String ASSIGNEE = "assignee";
    public static final String PREV_ASSIGNEE = "prevAssignee";
    public static final String ITEM_NAME_LOWER = "itemNameLower";

    public Item() {

    }

    public String getItemName() {
        return getString(ITEM_NAME);
    }

    public void setItemName(String itemName) {
        put(ITEM_NAME, itemName);
    }

    public String getSerialNumber() {
        return getString(SERIAL_NUMBER);
    }

    public void setSerialNumber(String serialNumber) {
        put(SERIAL_NUMBER, serialNumber);
    }

    public String getComments() {
        return getString(COMMENTS);
    }

    public void setComments(String comments) {
        put(COMMENTS, comments);
    }

    public User getAssignee() {
        return (User) getParseUser(ASSIGNEE);
    }

    public void setAssignee(User assignee) {
        put(ASSIGNEE, assignee);
    }

    public String getAssigneeName() {
        return getAssignee().getName();
    }

    public User getPrevAssignee() {
        return (User) getParseUser(PREV_ASSIGNEE);
    }

    public void setPrevAssignee(User user) {
        put(PREV_ASSIGNEE, user);
    }
}
