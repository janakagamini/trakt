package doe.pinky.trakt.models;

import com.parse.ParseClassName;
import com.parse.ParseUser;

@ParseClassName("_User")
public class User extends ParseUser {

    public final static String CLASS_NAME = "_User";
    public final static String NAME = "name";

    public User() {

    }

    public String getName() {
        return getString(NAME);
    }

    public void setName(String name) {
        put(NAME, name);
    }
}
