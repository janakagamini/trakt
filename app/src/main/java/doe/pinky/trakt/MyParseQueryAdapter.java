package doe.pinky.trakt;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parse.ParseQueryAdapter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import butterknife.Bind;
import butterknife.ButterKnife;
import doe.pinky.trakt.models.Item;

public class MyParseQueryAdapter extends ParseQueryAdapter<Item> {

    public MyParseQueryAdapter(Context context, QueryFactory<Item> queryFactory) {
        super(context, queryFactory);
    }



    @Override
    public View getItemView(Item item, View view, ViewGroup parent) {

        ViewHolder holder;

        if (view == null) {
            view = View.inflate(getContext(), R.layout.item_view, null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        holder.itemName.setText(item.getItemName());
        if (!item.getSerialNumber().equals("")) holder.serialNumber.setText(item.getSerialNumber());
        else holder.serialNumber.setText("N/A");

//        item.getAssignee(new GetCallback<User>() {
//            @Override
//            public void done(User user, ParseException e) {
//                if (e == null) {
//                    holder.assignee.setText("Assigned to: " + user.getName());
//                }
//            }
//        });

        holder.assignee.setText("Assigned to: " + item.getAssigneeName());

        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String updatedAt = df.format(item.getUpdatedAt());
        holder.lastUpdate.setText("Last updated: " + updatedAt);

        return view;
    }

    static class ViewHolder {
        @Bind(R.id.item_name)
        TextView itemName;
        @Bind(R.id.serial_number)
        TextView serialNumber;
        @Bind(R.id.assignee)
        TextView assignee;
        @Bind(R.id.last_updated)
        TextView lastUpdate;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

    }
}
