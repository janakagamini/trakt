package doe.pinky.trakt;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.LogOutCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;

import butterknife.Bind;
import butterknife.ButterKnife;
import doe.pinky.trakt.models.Item;

public class MainActivity extends AppCompatActivity {

    private final static int ITEM_REQUEST_CODE = 98;

    @Bind(R.id.item_list)
    ListView itemList;
    @Bind(R.id.empty)
    View empty;

    ProgressDialog progressDialog;

    MyParseQueryAdapter adapter;

    MenuItem showMine;
    MenuItem searchMenuItem;
    SearchView searchView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddItemActivity.class);
                startActivityForResult(intent, ITEM_REQUEST_CODE);
            }
        });

        adapter = new MyParseQueryAdapter(MainActivity.this, new ParseQueryAdapter.QueryFactory<Item>() {
            @Override
            public ParseQuery<Item> create() {
                ParseQuery<Item> query = new ParseQuery<>(Item.CLASS_NAME);
                query.orderByDescending(Item.UPDATED_AT);
                query.include(Item.ASSIGNEE);
                return query;
            }
        });
        itemList.setAdapter(adapter);
        itemList.setEmptyView(empty);
        itemList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Item item = adapter.getItem(position);

                if (ParseUser.getCurrentUser().getObjectId().equals(item.getAssignee().getObjectId())) {
                    Intent intent = new Intent(MainActivity.this, ViewItemActivity.class);
                    intent.putExtra(Item.OBJECT_ID, item.getObjectId());
                    startActivityForResult(intent, ITEM_REQUEST_CODE);
                } else {
                    AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Re-assign Item")
                            .setMessage(item.getItemName() + "\n" + item.getSerialNumber() + "\n\nYou cannot re-assign an item that isn't assigned to you.\n")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            })
                            .create();

                    alertDialog.show();
                }
            }
        });
    }

    @Override
    protected void onNewIntent(Intent intent) {

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            // Handle the normal search query case
            final String queryString = intent.getStringExtra(SearchManager.QUERY);
            adapter = new MyParseQueryAdapter(MainActivity.this, new ParseQueryAdapter.QueryFactory<Item>() {
                @Override
                public ParseQuery<Item> create() {
                    ParseQuery<Item> query = new ParseQuery<Item>(Item.CLASS_NAME);
                    query.whereContains(Item.ITEM_NAME_LOWER, queryString);
                    query.orderByDescending(Item.UPDATED_AT);
                    query.include(Item.ASSIGNEE);
                    return query;
                }
            });
            itemList.setAdapter(adapter);
        } else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            // Handle a suggestions click (because the suggestions all use ACTION_VIEW)
            String objectId = intent.getDataString();
            Log.d("DEBUG", "objectId: " + objectId);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ITEM_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                if (!searchView.isIconified()) searchView.setIconified(true);
                MenuItemCompat.collapseActionView(searchMenuItem);

                adapter = new MyParseQueryAdapter(MainActivity.this, new ParseQueryAdapter.QueryFactory<Item>() {
                    @Override
                    public ParseQuery<Item> create() {
                        ParseQuery<Item> query = new ParseQuery<>(Item.CLASS_NAME);
                        if(showMine.isChecked()) query.whereEqualTo(Item.ASSIGNEE, ParseUser.getCurrentUser());
                        query.orderByDescending(Item.UPDATED_AT);
                        query.include(Item.ASSIGNEE);
                        return query;
                    }
                });
                itemList.setAdapter(adapter);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        showMine = menu.findItem(R.id.action_show_mine);
        searchMenuItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(searchMenuItem);
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        MenuItemCompat.setOnActionExpandListener(menu.findItem(R.id.action_search), new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                adapter = new MyParseQueryAdapter(MainActivity.this, new ParseQueryAdapter.QueryFactory<Item>() {
                    @Override
                    public ParseQuery<Item> create() {
                        ParseQuery<Item> query = new ParseQuery<>(Item.CLASS_NAME);
                        query.orderByDescending(Item.UPDATED_AT);
                        query.include(Item.ASSIGNEE);
                        return query;
                    }
                });
                itemList.setAdapter(adapter);
                return true;
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_sign_out) {

            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Signing out...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            ParseUser.logOutInBackground(new LogOutCallback() {
                @Override
                public void done(ParseException e) {

                    progressDialog.dismiss();

                    if (e == null) {
                        Intent intent = new Intent(MainActivity.this, DispatchActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    } else {
                        Toast.makeText(MainActivity.this, "Could not sign out, try again later.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
            return true;
        } else if (id == R.id.action_show_mine) {
            if (item.isChecked()) {
                item.setChecked(false);
                adapter = new MyParseQueryAdapter(MainActivity.this, new ParseQueryAdapter.QueryFactory<Item>() {
                    @Override
                    public ParseQuery<Item> create() {
                        ParseQuery<Item> query = new ParseQuery<>(Item.CLASS_NAME);
                        query.orderByDescending(Item.UPDATED_AT);
                        query.include(Item.ASSIGNEE);
                        return query;
                    }
                });
                itemList.setAdapter(adapter);
            } else {
                item.setChecked(true);
                adapter = new MyParseQueryAdapter(MainActivity.this, new ParseQueryAdapter.QueryFactory<Item>() {
                    @Override
                    public ParseQuery<Item> create() {
                        ParseQuery<Item> query = new ParseQuery<>(Item.CLASS_NAME);
                        query.whereEqualTo(Item.ASSIGNEE, ParseUser.getCurrentUser());
                        query.orderByDescending(Item.UPDATED_AT);
                        query.include(Item.ASSIGNEE);
                        return query;
                    }
                });
                itemList.setAdapter(adapter);
            }
        }

        return super.onOptionsItemSelected(item);
    }
}
